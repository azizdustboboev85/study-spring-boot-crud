package dars5.vazifa1.service;

import dars5.vazifa1.domen.Room;
import dars5.vazifa1.repository.RoomRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;


@Service
public class RoomService {

    @Autowired
    private RoomRepo roomRepo;
    //Create

    public Room add(Room room) {
        return roomRepo.save(room);
    }

    //Read
    public Room get(Long id) {
        Optional<Room> optionalRoom = roomRepo.findById(id);

        if (optionalRoom.isPresent()) {
            Room room = optionalRoom.get();
            return room;
        }
        return null;
    }


//    Read All
    public List<Room> getAllList() {
        return roomRepo.findAll();
    }


//    update
    public Room editroom(Room room) {
        if (room.getId() == null) {
            throw new RuntimeException("Room's id is not be NULL!");
        }
        Optional<Room> optionalRoom = roomRepo.findById(room.getId());
        if (!optionalRoom.isPresent()) {
            throw new RuntimeException("Rooms not found!");
        }
        return roomRepo.save(room);
    }

//    delete
    public void deleteroom(Long id) {

        Optional<Room> optionalRoom = roomRepo.findById(id);
        if (!optionalRoom.isPresent()) {
            throw new RuntimeException("Room not found!");
        }
        Room room = optionalRoom.get();

        roomRepo.delete(room);

    }
}
