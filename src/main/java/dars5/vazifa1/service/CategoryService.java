package dars5.vazifa1.service;

import dars5.vazifa1.domen.Category;
import dars5.vazifa1.repository.CategoryRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class CategoryService {

    @Autowired
    private CategoryRepo categoryRepo;

    public Category add(Category category) {
        return categoryRepo.save(category);

    }

    public Category get(Long id) {
        Optional<Category> optionalCategory = categoryRepo.findById(id);
        if (optionalCategory.isPresent()) {
            Category category = optionalCategory.get();
            return category;
        }
        return null;
    }


    public List<Category> getAll() {
        return categoryRepo.findAll();
    }

    public Category edit(Category category) {
        if (Objects.isNull(category.getId())) {
            throw new RuntimeException("Category id shoud not be null!");
        }
        Optional<Category> optionalCategory = categoryRepo.findById(category.getId());
        if (!optionalCategory.isPresent()) {
            throw new RuntimeException("Category not found!");
        }

        return categoryRepo.save(category);
    }

    public void delete(Long id) {

        Optional<Category> optionalCategory = categoryRepo.findById(id);
        if (!optionalCategory.isPresent()) {
            throw new RuntimeException("Category not found!");
        }
        Category category = optionalCategory.get();

        categoryRepo.delete(category);
    }

}
