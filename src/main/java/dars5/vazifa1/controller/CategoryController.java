package dars5.vazifa1.controller;

import dars5.vazifa1.domen.Category;
import dars5.vazifa1.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/category")
public class CategoryController {

    //create

    @Autowired
    private CategoryService categoryService;

    @PostMapping("/add")
    public Category add(@RequestBody Category category){
        return categoryService.add(category);

    }

//    Read by id
    @GetMapping("/get/{id}")
    public Category get(@PathVariable(value = "id") Long id){
        return categoryService.get(id);

    }

//    Read all
    @GetMapping("/get/list")
    public List<Category> getAll(){
        return categoryService.getAll();

    }

//    update
    @PutMapping("/edit")
    public Category edit(@RequestBody Category category){
        return categoryService.edit(category);
    }


//    delete
    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable(value = "id") Long id) {
        categoryService.delete(id);
    }
}
