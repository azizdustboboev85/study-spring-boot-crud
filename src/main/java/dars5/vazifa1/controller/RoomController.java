package dars5.vazifa1.controller;

import dars5.vazifa1.domen.Room;
import dars5.vazifa1.service.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/room")
public class RoomController {

    @Autowired
    private RoomService roomService;

    @PostMapping("/add")
    public Room add(@RequestBody Room room){
        return roomService.add(room);

    }

    @GetMapping("/get/{id}")
    public Room get(@PathVariable(value = "id") Long id){
        return roomService.get(id);
    }

    @GetMapping("/get/all")
    public List<Room> getAll() {
        return roomService.getAllList();
    }

    @PutMapping("/edit")
    public Room edit(@RequestBody Room room) {
        return roomService.editroom(room);
    }

    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable(value = "id") Long id){
        roomService.deleteroom(id);

    }
}
