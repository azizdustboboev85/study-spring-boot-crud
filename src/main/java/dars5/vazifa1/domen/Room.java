package dars5.vazifa1.domen;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "rooms")
public class Room {

    @Id  //primary key
    @GeneratedValue(strategy = GenerationType.IDENTITY)  //sqeunce
    @Column(name = "id")
    private Long id;

    @Column(name = "number", nullable = false, unique = true)
    private Integer number;

//    @Column(name = "category_id", nullable = false)
//    private Long categoryId;

    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category;

    @Column(name = "floor")
    private Short floor;

    @Column(name = "price")
    private Double price;

    @Column(name = "hold_price")
    private Double holdPrice;
}
